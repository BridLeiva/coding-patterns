#!/usr/env python

#let's define the types of helmets.
class Round_Helmet:
    def __str__(self): return "round"
    
class Pointy_Helmet:
    def __str__(self): return "pointy"

#Defining the types of weapons.
class Spear:
    def __str__(self): return "spear"

class Sword:
    def __str__(self): return "sword"
    
#And the types of armor.
class Shield:
    def __str__(self): return "shoulder shield"
    
class Chain_Mail:
    def __str__(self): return "chain mail"
    
# Finally let's put it all together in our abstract factory, the medieval knight class.
class Medieval_Knight:
    def __init__(self, helmet, bodyArmor, weapon):
        self.helmet = self.setHelmet(helmet)
        self.bodyArmor = self.setArmor(bodyArmor)
        self.weapon = self.setWeapon(weapon)
        
    def __str__(self):
        return """A medieval knight, using a {armor} as body armor, a {helmet} helmet, 
        and a {weapon} as his weapon of choice."""\
                .format(helmet=self.helmet, armor=self.bodyArmor, weapon=self.weapon)
        
    def setHelmet(self,helmetType): 
        helmetTypes = {
            'pointy': Pointy_Helmet
            , 'round': Round_Helmet
        }
        
        return helmetTypes[helmetType.lower()]()
    
    def setWeapon(self,weaponType):
        weaponTypes = {
            'spear': Spear
            , 'sword': Sword
        }
        
        return weaponTypes[weaponType.lower()]()
    
    def setArmor(self,armorType):
        armorTypes = {
            'shield': Shield
            , 'mail': Chain_Mail
        }
        
        return armorTypes[armorType.lower()]()

# And again, we can test it in our main function.
def main():
    lightKnight =  Medieval_Knight('round','mail','sword')
    print(lightKnight)
    
if __name__ == '__main__':
    main()