#!/usr/bin/env python

# let's look first at the disperse elements that constitute a cellphone
# and I think I don't need to tell you that this is a much simplistic
# view of such device :)

class Screen:
    def __str__(self): return "Activating the screen."

class GPS:
    def __str__(self): return "Implementing geolocalization."

class Microphone:
    def __str__(self): return "Microphone ready."

class Speaker:
    def __str__(self): return "Activating speaker."

class Accelerometers:
    def __str__(self):
        return "Tracking the spacial orientation of the phone."

# I would be a daunting task for the user if they had to manage
# every one of these aspects of the device every time they need to
# do something with it. So we need an interface that simplifies thing
# while allowing access to all the components of the device, let's
#  call it... an OS!

class OS:
    def makeCall(self):
        print ("Starting a call...")
        print (Microphone())
        print (Speaker())

        print ("Dialing...")

    def playGame(self):
        print ("Starting selected game...")
        print (Screen())
        print (Accelerometers())
        print (Speaker())

        print ("Have fun!")

if __name__=='__main__':
    facade = OS()
    facade.playGame()
