#!/usr/bin/env python

# We want a decorator that allows us to print on the screen the name of the
# it's about to be executed, say, for logging or to inform the user about what
# is the software doing without contaminating our functions with extra code.
def printer(fn):
    def decoratedFunction(*args):
        print("Applying {}: ".format(fn.__name__))

        return fn(*args)

    return decoratedFunction

# We can test it in our main function.
def main():
    @printer
    def newFunction():
        print("Body of the function.")

    newFunction()

if __name__=='__main__': main()
