#!/usr/bin/env python

#Assuming we have different chess pieces, and we want a factory that returns a given piece by name.

#let's first define our pieces:
class pawn:
    def __str__(self): return "The soul of the game."
        
class knight:
    def __str__(self): return "A brave Knight."
        
class rook:
    def __str__(self): return "Impenetrable rook."

# Now, here is where the magic happens, the following function is our factory method.
def getPieceByName(pieceName):
    if pieceName == "knight":
        piece = knight()
    elif pieceName == "rook":
        piece = rook()
    else:
        piece = pawn()
    
    return piece

# In the main function we can test our factory.
def main():
    pawn = getPieceByName('pawn')
    knight = getPieceByName('knight')    
    rook = getPieceByName('rook')

    print(pawn)
    print('-------')
    print(knight)
    print('-------')
    print(rook)
    print('-------')

if __name__ == '__main__':
    main()