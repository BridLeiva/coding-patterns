#!/usr/bin/env python

#the car object that will be returned after the building process
class car:
    def __str__(self):
        return "A {color} {type}.".format(
            color = self.color
            , type = self.type
        )

class carBuilder:
    """A mother class for the different types of builders"""
    def __init__(self):
        self.car = car()

    def ensembleMotor(self):
        print ("Assembling motor.")
        print ("Placing motor on car.")

    def ensembleChasis(self):
        print ("Assembling chasis.")

    def paint(self, color):
        print("Painting: {}".format(color))
        self.car.color = color

    def createInternalSystems(self):
        print("Ensembling mechanical system.")
        print("Ensembling electrical system.")

    def attachWeels(self):
        print("Attaching the weels.")

# Defining two builders, one for sedans and the other for pick-ups.
class sedanBuilder(carBuilder):
    def ensembleChasis(self):
        super().ensembleChasis()
        self.car.type = "sedan"

class pickUpBuilder(carBuilder):
    def ensembleChasis(self):
        super().ensembleChasis()
        self.car.type = "pick-up"

class carFactory: # Not to confuse with the factory pattern!
    def __init__(self, builder):
        print ("Building a red car...")

        builder.ensembleChasis()
        builder.ensembleMotor()
        builder.createInternalSystems()
        builder.attachWeels()
        builder.paint('red')

        self.finishedCar = builder.car

    def getCar(self): return self.finishedCar

def main():
    carFact = carFactory(sedanBuilder())
    print ("--------------------")
    print (carFact.getCar())

if __name__ == '__main__':
    main()
