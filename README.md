# README #

This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/).

This project is not meant to be an application, it's only a place where I keep my notes on coding patterns.

Please share any error report, typo, suggestion, or your personal story in the [project public repo](https://gitlab.com/BridLeiva/coding-patterns).
