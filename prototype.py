#!/usr/bin/env python

import copy as cp
import random as rn # for our genetic algorithm

class organism:
    def __init__(self, genome):
        self.genome = genome
        self.mutability = 0.05 # this will affect the probability that the genome will mutate.

    # prototyping is only one of the things that can be done with the copy module
    def clone(self): return cp.deepcopy(self) # note that cp.copy() would have
            # created a shallow copy of the object.

    def mutate(self):
        def switch(val): return '1' if val == '0' else '0'

        genomeAsList = [ switch(x) if rn.uniform(0,1) < self.mutability else x
                                    for x in self.genome ]

        self.genome = ''.join(genomeAsList)

        return self

#In our main function, we can implement a small genetic algorithm.
def main():
    targetGenome = \
        '111001001001111100110010001010111111001111001100001100111010010011001100110000110001101100110000110001000100011001100'

    def randomGenome(long):
        """generates a random genome of a given lenght"""
        genomeAsList = [ rn.choice(('0','1')) for i in range(0,long) ]
        return ''.join(genomeAsList)

    def fitness(org):
        """Returns the fitness of a given organism as a function of the coincidences
        between the organisms genome and the target genome."""
        coincidences = [ 1.0 if val == targetGenome[i] else 0.0 for (i, val) in enumerate(org.genome) ]
        return sum(coincidences)/len(targetGenome)

    # let´s generate a population of 100 individuals
    population = [ organism(randomGenome(len(targetGenome))) for i in range(0,100) ]

    fittestOrg = population[-1] # The fitterst organism, for now.
    maxFitness = 0
    generation = 0

    while maxFitness < 0.99:
        # Now we put them to fight for their life, because reasons...
        for idx, individual in enumerate(population):
            contestantIdx = rn.randint( 0, len(population) -1 ) # This is necesary if the contestant loses.
            contestant = population[contestantIdx]
            # ------------------------------------------------------
            if ( fitness(individual) > fitness(contestant) ):
                population[contestantIdx] = individual.clone().mutate()

                fittestOrg = individual if ( fitness(individual) > fitness(fittestOrg) ) else fittestOrg

            elif ( fitness(individual) < fitness(contestant) ):
                population[idx] = contestant.clone().mutate()

                fittestOrg = contestant if ( fitness(contestant) > fitness(fittestOrg) ) else fittestOrg

            #-------------------------------------------------------
        # fitnessList = [ fitness( x ) for x in population ]
        # maxFitness = max(fitnessList)
        maxFitness = fitness(fittestOrg)
        generation += 1

        print ("MaxFitness: {0} - generation: {1}".format(maxFitness, generation))

    print (targetGenome)
    print (fittestOrg.genome)

if __name__ == '__main__': main()
