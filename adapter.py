#!/usr/bin/env python

from datetime import datetime as dt

# Let's say we have the following function, and we can't edit it.
def currHour(): return dt.now().hour

# so we write a wrapper:
def currHourWrapper(frmt = '12h'):
    hour24 = currHour() # the hour in 24h format.

    # If that's what we are asked for, just return.
    if frmt == '24h': return hour24

    # Else we return it on a 12h format.
    return hour24 if hour24 < 12 else hour24 - 12

# And we can test it in our main function
def main():
    print(currHourWrapper())
    print(currHourWrapper('24h'))

if __name__ == '__main__': main()
